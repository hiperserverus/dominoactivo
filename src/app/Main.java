/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javax.swing.UIManager;
import javax.swing.JOptionPane;
import models.Sell;
import views.Principal;
import controllers.SellController;

/**
 *
 * @author Machine
 */
public class Main {

    public static void main(String args[]) {

        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }

            Sell sellModel = new Sell();
            Principal principalView = new Principal();
            SellController sellController = new SellController(sellModel, principalView);
            sellController.Iniciar();

    }

}
