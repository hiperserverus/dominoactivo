
package models;




import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import views.Principal;

/**
 *
 * @author Machine
 */
public class Sell {
public static DefaultTableModel modelo = new DefaultTableModel();
private Principal principalView = new Principal();
public String[] listTimes =  {"10:00 AM", "11:00 AM", "12:00 PM", "01:00 PM", "04:00 PM", "05:00 PM", "06:00 PM", "07:00 PM"};
public String[] listChips = {"0:0", "0:1", "0:2", "0:3", "0:4", "0:5", "0:6",
                             "1:0", "1:1", "1:2", "1:3", "1:4", "1:5", "1:6",
                             "2:0","2:1", "2:2", "2:3", "2:4", "2:5", "2:6",
                             "3:0","3:1", "3:2", "3:3", "3:4", "3:5", "3:6",
                             "4:0","4:1", "4:2", "4:3", "4:4", "4:5", "4:6",
                             "5:0","5:1", "5:2", "5:3", "5:4", "5:5", "5:6",
                             "6:0","6:1", "6:2", "6:3", "6:4", "6:5", "6:6"};


   public void  loadMethods() {
        showTable();
        addTimes();
        addChips();
 }

 void showTable() {
      modelo.addColumn("Sorteo");
      modelo.addColumn("Jugada");
      modelo.addColumn("Monto");
      principalView.tblTicket.setModel(modelo);
 }

 void addTimes() {

     JLabel times[] = {principalView.lblTime1,principalView.lblTime2,principalView.lblTime3,principalView.lblTime4,
                        principalView.lblTime5,principalView.lblTime6,principalView.lblTime7,principalView.lblTime8};

       for(int i = 0; i < this.listTimes.length; i++) {

            times[i].setText(this.listTimes[i]);
   }

 }

  void addChips(){

JLabel chips[] = {principalView.lbl1,principalView.lbl2,principalView.lbl3,principalView.lbl4,principalView.lbl5,principalView.lbl6,principalView.lbl7
                    ,principalView.lbl8,principalView.lbl9,principalView.lbl10,principalView.lbl11,principalView.lbl12,principalView.lbl13,principalView.lbl14
                    ,principalView.lbl15,principalView.lbl16,principalView.lbl17,principalView.lbl18,principalView.lbl19,principalView.lbl20,principalView.lbl21
                    ,principalView.lbl22,principalView.lbl23,principalView.lbl24,principalView.lbl25,principalView.lbl26,principalView.lbl27,principalView.lbl28
                    ,principalView.lbl29,principalView.lbl30,principalView.lbl31,principalView.lbl32,principalView.lbl33,principalView.lbl34,principalView.lbl35
                    ,principalView.lbl36,principalView.lbl37,principalView.lbl38,principalView.lbl39,principalView.lbl40,principalView.lbl41,principalView.lbl42
                    ,principalView.lbl43,principalView.lbl44,principalView.lbl45,principalView.lbl46,principalView.lbl47,principalView.lbl48,principalView.lbl49};

   for(int i = 0; i < this.listChips.length; i++) {

     chips[i].setText(this.listChips[i]);
   }
 
 }


 public void addPlay(String[] data) {
        modelo.addRow(data);
 }








}
