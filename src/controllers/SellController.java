
package controllers;




import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import models.Sell;
import views.Principal;

/**
 *
 * @author Machine
 */
public class SellController implements ActionListener {
    private final int timesLimit = 8;
    private final int limit = 49;
    private final int minBet = 100;
    public boolean chipsSelected[] = new boolean[limit];
    public boolean timesSelected[] = new boolean[timesLimit];
    private Sell sellModel;
    public LinkedList listPlay = new LinkedList();

    private Principal principalView;
     JLabel times[] = {principalView.lblTime1,principalView.lblTime2,principalView.lblTime3,principalView.lblTime4,
                        principalView.lblTime5,principalView.lblTime6,principalView.lblTime7,principalView.lblTime8};

    JLabel chips[] = {principalView.lbl1,principalView.lbl2,principalView.lbl3,principalView.lbl4,principalView.lbl5,principalView.lbl6,principalView.lbl7
                    ,principalView.lbl8,principalView.lbl9,principalView.lbl10,principalView.lbl11,principalView.lbl12,principalView.lbl13,principalView.lbl14
                    ,principalView.lbl15,principalView.lbl16,principalView.lbl17,principalView.lbl18,principalView.lbl19,principalView.lbl20,principalView.lbl21
                    ,principalView.lbl22,principalView.lbl23,principalView.lbl24,principalView.lbl25,principalView.lbl26,principalView.lbl27,principalView.lbl28
                    ,principalView.lbl29,principalView.lbl30,principalView.lbl31,principalView.lbl32,principalView.lbl33,principalView.lbl34,principalView.lbl35
                    ,principalView.lbl36,principalView.lbl37,principalView.lbl38,principalView.lbl39,principalView.lbl40,principalView.lbl41,principalView.lbl42
                    ,principalView.lbl43,principalView.lbl44,principalView.lbl45,principalView.lbl46,principalView.lbl47,principalView.lbl48,principalView.lbl49};

        public SellController(Sell sellModel, Principal principalView ){

        this.sellModel = sellModel;
        this.principalView = principalView;
        this.principalView.btnAdd.addActionListener(this);
        this.principalView.cbAll.addActionListener(this);
        eventPaintTimes();
        eventPaintChips();
        eventKeyTypedNumber();
        eventkeyTypedBet();
        eventKeyPrint();

    }


    public void Iniciar() {

        principalView.setTitle("newApp");
        principalView.pack();
        principalView.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        principalView.setLocationRelativeTo(null);
        principalView.setVisible(true);
        sellModel.loadMethods();
        initializerTimesSelected();
        initializerTimesSelected();

    }

    public void actionPerformed(ActionEvent e){

        if(principalView.btnAdd == e.getSource()) {

            try{

                    String timeDraw[] = new String[timesLimit];
                    String playDraw[] = new String[limit];
                  

//////////////////////////////////////////////////////////
                    timeDraw = timesSelecteds(timeDraw);
//////////////////////////////////////////////////////////
                    playDraw = chipsSelecteds(playDraw);
//////////////////////////////////////////////////////////
    if( principalView.txtBet.getText().equals("")){
           JOptionPane.showMessageDialog(null,"DEBE INGRESAR UN MONTO DE APUESTA","ESTIMADO USUARIO",JOptionPane.WARNING_MESSAGE) ;
      } else {

                if( Integer.parseInt(principalView.txtBet.getText()) >= minBet)  {

                  /* Metodo para recolectar las horas y los numeros seleccionados.
                     Ademas modela la tabla de acuerdos a los datos seleccionados */

                    collectingData(timeDraw, playDraw);

                    //Aqui se limpian los numeros seleccionados y se reinicia el pintado de los labels
                     initializerChipsSelected();
                     clearPaintChips();
                     
                     //VACIAR JUGADAS EN LA TABLA

                     injectData();

                }else{

                     JOptionPane.showMessageDialog(null,"El monto minimo de apuesta es : "+ minBet + " Bs.F","ESTIMADO USUARIO",JOptionPane.INFORMATION_MESSAGE) ;
                }

          }

            }catch(Exception ex){

            }

        }
////////////////////////////////////////////////////////////////////////////////
// EVENTO DE ACTION PARA EL SELECCIONADO DE TODAS LAS HORAS DE LOS SORTEOS

                 if(principalView.cbAll == e.getSource()) {
                     
            try{

               if( principalView.cbAll.isSelected()) {
                for (int i = 0; i < times.length; i++) {
                timesSelected[i] = true;
                times[i].setBackground(Color.BLACK);
                times[i].setForeground(Color.WHITE);
                   }
              } else {
                   for (int i = 0; i < times.length; i++) {
                timesSelected[i] = false;
                times[i].setBackground(Color.WHITE);
                times[i].setForeground(Color.BLACK);
                   }
              }

            }catch(Exception ex){

            }
        }

    }

    public void eventPaintTimes(){
                    for( int i = 0; i < times.length; i++) {

             times[i].addMouseListener(new java.awt.event.MouseAdapter() {

                @Override
                 public void mouseClicked(java.awt.event.MouseEvent evt) {
                            modelPaint(evt, times);
                      }
                  });
            }
    }

   public void eventPaintChips(){
                   for( int i = 0; i < chips.length; i++) {

            chips[i].addMouseListener(new java.awt.event.MouseAdapter() {

                @Override
                 public void mouseClicked(java.awt.event.MouseEvent evt) {
                            modelPaint(evt, chips);
                      }
                  });
            }
   }

   public void clearPaintChips(){
                for (int i = 0; i < chips.length; i++) {

                chips[i].setBackground(Color.WHITE);
                chips[i].setForeground(Color.BLACK);

       }
   }


    public void modelPaint(MouseEvent e, JLabel list[]) {

         for (int i = 0; i < list.length; i++) {

          if(list[i] == e.getSource()) {
            try{

               if( list[i].getBackground().equals(Color.WHITE)) {
                if(list.length == times.length) {
                    timesSelected[i] = true;
                }else{
                    chipsSelected[i] = true;
                }
                list[i].setBackground(Color.BLACK);
                list[i].setForeground(Color.WHITE);
              } else {
                if(list.length == times.length) {
                    timesSelected[i] = false;
                }else{
                    chipsSelected[i] = false;
                }
                list[i].setBackground(Color.WHITE);
                list[i].setForeground(Color.BLACK);
              }
            }catch(Exception ex){

            }
        }

       }
    }



void initializerTimesSelected(){

    for (int i = 0; i < this.timesSelected.length; i++) {

        timesSelected[i] = false;
    }
}

void initializerChipsSelected(){

    for (int i = 0; i < this.chipsSelected.length; i++) {

        chipsSelected[i] = false;
    }
}

public String[] timesSelecteds(String timeDraw[]){
                  for (int i = 0; i < timeDraw.length; i++) {

                   if (timesSelected[i] == true) {

                       timeDraw[i] = times[i].getText();
                   }

               }
          return timeDraw;
}

public String[] chipsSelecteds(String playDraw[]){

                  for (int i = 0; i < playDraw.length; i++) {

                   if (chipsSelected[i] == true) {

                       playDraw[i] = chips[i].getText();
                   }

               }
          return playDraw;
}

void collectingData(String timeDraw[],String playDraw[]){

       for (int i = 0; i < timeDraw.length; i++) {

            if (timeDraw[i] != null) {

                for (int j = 0; j < playDraw.length; j++) {

                        if (playDraw[j] != null) {

                             if (!isInList(listPlay, playDraw[j], timeDraw[i])){

                                   listPlay.offer(timeDraw[i]);
                                   listPlay.offer(playDraw[j]);
                                   listPlay.offer(principalView.txtBet.getText());
                                   sellModel.addPlay(null);

                                   }
                                  }
                               }
                            }
                     }
}

void collectingDataSingle(String timeDraw[], String playDraw){

       for (int i = 0; i < timeDraw.length; i++) {

            if (timeDraw[i] != null) {

                if (!isInList(listPlay, playDraw, timeDraw[i])){

               listPlay.offer(timeDraw[i]);
               listPlay.offer(playDraw);
               listPlay.offer(principalView.txtBet.getText());
               sellModel.addPlay(null);
                }
              }
            }
}

void injectData(){

    for(int f = 0; f < principalView.tblTicket.getRowCount(); f++) {

         for(int c = 0; c < principalView.tblTicket.getColumnCount(); c++) {

                     listPlay.offer(listPlay.getFirst());
                     principalView.tblTicket.setValueAt(listPlay.pop(), f, c);

                }
          }
}

public  boolean isInList (LinkedList list, String play, String time){

    for (int i = 0; i < list.size(); i++) {

        if(list.get(i).toString().equals(play)){
            if(list.get(i-1).toString().equals(time)){
                return true;
            }
            
        }
    }


    return false;
}

void eventKeyTypedNumber() {
    
    principalView.txtNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {

             char c=evt.getKeyChar();
             int sizeValue = principalView.txtNumber.getText().length();

                 if(!Character.isDigit(c)){
                      evt.consume();

                    }else{
                         if( Integer.parseInt(String.valueOf(c)) >6 ){

                                evt.consume();

                           }else{

                             if (sizeValue == 0){

                                principalView.txtNumber.setText(String.valueOf(c)+":");
                                evt.consume();
                             }

                                if( sizeValue >= 3) {

                                 evt.consume();
                             }

                           }
                         }

                       if ( c == 8 && sizeValue == 1){
                                 System.out.print("entro");
                                 principalView.txtNumber.setText("");
                             }

                        if ( c == 10 && sizeValue == 3){
                                 principalView.txtNumber.transferFocus();
                             }
                            }
        }); 
}

void eventkeyTypedBet() {

    principalView.txtBet.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {

             char c=evt.getKeyChar();
             int sizeValue = principalView.txtBet.getText().length();

                 if(!Character.isDigit(c)){
                      evt.consume();

                    }

                  if (sizeValue >= 6){
                      evt.consume();
                  }

                   if ( c == 10 ){

                         String timeDraw[] = new String[timesLimit];

                         timeDraw = timesSelecteds(timeDraw);

                         collectingDataSingle(timeDraw,  principalView.txtNumber.getText());

                         injectData();

                         principalView.txtNumber.setText("");

                         principalView.txtBet.setText("");

                         principalView.txtBet.transferFocus();
                     
                      }
                }
        });
}

void eventKeyPrint() {

    principalView.btnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
            
             char c=evt.getKeyChar();
              if ( evt.getKeyCode() == java.awt.event.KeyEvent.VK_F12 ){

                JOptionPane.showMessageDialog(null,"Imprimiendo...","TICKET CREADO",JOptionPane.INFORMATION_MESSAGE) ;

                      }
                }
        });
}

}
